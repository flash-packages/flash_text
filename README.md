# flash_text

[![pub package](https://img.shields.io/pub/v/flash_text.svg)](https://pub.dev/packages/flash_text)

A Flutter plugin for text theming.
Supports Android, iOS, Linux, macOS and Windows.
Not all methods are supported on all platforms.

|             | Android | iOS  | Linux | macOS  | Windows     |
|-------------|---------|------|-------|--------|-------------|
| **Support** | SDK 16+ | 9.0+ | Any   | 10.11+ | Windows 10+ |

## Usage

To use this plugin, add `flash_text` as a [dependency in your pubspec.yaml file](https://flutter.dev/docs/development/platform-integration/platform-channels).

## Example
<?code-excerpt "readme_excerpts.dart (Example)"?>
```dart
class MyScreen extends StatefulWidget {
  const MyScreen({super.key});

  @override
  State<MyScreen> createState() => _MyScreenState();
}

class _MyScreenState extends State<MyScreen> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const FlashText(
          title: "Flash Text",
          maxLine: 2,
          style: TextStyle(
            fontWeight: FontWeight.w600,
          ),
        ),
        FlashText(
          title: "Flash Text Button",
          maxLine: 2,
          onTap: () {
            // If onTap action given then it will build a button
            // onTap action goes here
          },
          style: const TextStyle(
            fontWeight: FontWeight.w600,
          ),
        ),
      ],
    );
  }
}
```


See this `flash_text` [test](https://github.com/flutter/packages/blob/main/packages/flash_text/flash_text/test/flash_text.dart) for an example.

A Codeflash's Product ✨