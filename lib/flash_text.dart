library flash_text;

import 'package:flutter/material.dart';

class FlashText extends StatelessWidget {
  final String? title;
  final TextStyle? style;
  final TextAlign? align;
  final Alignment? btnTextAlignment;
  final int? maxLine;
  final TextOverflow? textOverflow;
  final void Function()? onTap;

  const FlashText({
    super.key,
    this.title,
    this.style,
    this.align = TextAlign.start,
    this.maxLine,
    this.textOverflow,
    this.onTap,
    this.btnTextAlignment,
  });

  /// Build stateless widget
  @override
  Widget build(BuildContext context) {
    final Widget textWidget = Text(
      title ?? "",
      style: style,
      softWrap: true,
      textAlign: align,
      maxLines: maxLine,
      overflow: textOverflow,
    );

    /// Check it onTap is given, if given then show text button
    return onTap == null
        ? textWidget
        : TextButton(
            onPressed: onTap,
            child: Align(
              /// If alignment if provided then use it else use center alignment
              alignment: btnTextAlignment ?? Alignment.center,
              child: textWidget,
            ),
          );
  }
}
