import 'package:flash_text/flash_text.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) => const MaterialApp(
        home: MyScreen(),
      );
}

class MyScreen extends StatefulWidget {
  const MyScreen({super.key});

  @override
  State<MyScreen> createState() => _MyScreenState();
}

class _MyScreenState extends State<MyScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Flash Text Example 1'),
      ),
      body: Column(
        children: [
          const FlashText(
            title: "Flash Text",
            maxLine: 2,
            style: TextStyle(
              fontWeight: FontWeight.w600,
            ),
          ),
          FlashText(
            title: "Flash Text",
            maxLine: 2,
            onTap: () {
              // onTap action goes here
            },
            style: const TextStyle(
              fontWeight: FontWeight.w600,
            ),
          ),
        ],
      ),
    );
  }
}
