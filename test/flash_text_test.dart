import 'package:flash_text/flash_text.dart';
import 'package:flutter/material.dart';

void main() {}

class MyScreen extends StatefulWidget {
  const MyScreen({super.key});

  @override
  State<MyScreen> createState() => _MyScreenState();
}

class _MyScreenState extends State<MyScreen> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const FlashText(
          title: "Flash Text",
          maxLine: 2,
          style: TextStyle(
            fontWeight: FontWeight.w600,
          ),
        ),
        FlashText(
          title: "Flash Text",
          maxLine: 2,
          onTap: () {
            // onTap action goes here
          },
          style: const TextStyle(
            fontWeight: FontWeight.w600,
          ),
        ),
      ],
    );
  }
}
