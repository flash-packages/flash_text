## 0.0.1

-   Initial release

## 0.0.2

-   Update readme
-   Add example

## 0.0.3

-   Rebranding to FlashText

## 0.0.4

-   Change metadata
